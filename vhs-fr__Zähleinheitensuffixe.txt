#separator:tab
#html:true
#guid column:1
#notetype column:2
#deck column:3
#tags column:8
C/f%*P{~ca	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ひとつ	1 (Gegenstände)	ひとつ[<br>hitotsu]		Gegenstände
k=rI14wvSM	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ふたつ	2 (Gegenstände)	ふたつ[<br>futatsu]		Gegenstände
u0}odMj*@3	Japanese Urania	vhs-fr::Zähleinheitensuffixe	みっつ	3 (Gegenstände)	みっつ[<br>mittsu]		Gegenstände
M;<kf0t4px	Japanese Urania	vhs-fr::Zähleinheitensuffixe	よっつ	4 (Gegenstände)	よっつ[<br>yottsu]		Gegenstände
e4m<4ZeIV7	Japanese Urania	vhs-fr::Zähleinheitensuffixe	いつつ	5 (Gegenstände)	いつつ[<br>itsutsu]		Gegenstände
f9-yhiAkaH	Japanese Urania	vhs-fr::Zähleinheitensuffixe	むっつ	6 (Gegenstände)	むっつ[<br>muttsu]		Gegenstände
e7Nj*I3i/{	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ななつ	7 (Gegenstände)	ななつ[<br>nanatsu]		Gegenstände
o54-M>5b4$	Japanese Urania	vhs-fr::Zähleinheitensuffixe	やっつ	8 (Gegenstände)	やっつ[<br>yattsu]		Gegenstände
m%p=k7FRJW	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ここのつ	9 (Gegenstände)	ここのつ[<br>kokonotsu]		Gegenstände
cfaC|dMR7F	Japanese Urania	vhs-fr::Zähleinheitensuffixe	とお	10 (Gegenstände)	とお[<br>tō]		Gegenstände
mH$,`?DGsX	Japanese Urania	vhs-fr::Zähleinheitensuffixe	いくつ	? (Gegenstände)	いくつ[<br>ikutsu]		Gegenstände
NpPNs8[!X7	Japanese Urania	vhs-fr::Zähleinheitensuffixe	いっさい	1 (Alter)	いっさい[<br>issai]		Alter
rG;&o(=p*u	Japanese Urania	vhs-fr::Zähleinheitensuffixe	にさい	2 (Alter)	にさい[<br>nisai]		Alter
P!NBLe*F[x	Japanese Urania	vhs-fr::Zähleinheitensuffixe	さんさい	3 (Alter)	さんさい[<br>sansai]		Alter
lPQNT4qNoH	Japanese Urania	vhs-fr::Zähleinheitensuffixe	よんさい	4 (Alter)	よんさい[<br>yonsai]		Alter
K60Lu+e^En	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ごさい	5 (Alter)	ごさい[<br>gosai]		Alter
g2LIP~|HJ`	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ろくさい	6 (Alter)	ろくさい[<br>rokusai]		Alter
K}AY5zG$~w	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ななさい	7 (Alter)	ななさい[<br>nanasai]		Alter
j)78n1!_K6	Japanese Urania	vhs-fr::Zähleinheitensuffixe	はっさい	8 (Alter)	はっさい[<br>hassai]		Alter
lumvCm`Eov	Japanese Urania	vhs-fr::Zähleinheitensuffixe	きゅうさい	9 (Alter)	きゅうさい[<br>kyūsai]		Alter
8/7-El`hq	Japanese Urania	vhs-fr::Zähleinheitensuffixe	じゅっさい<br>じっさい	10 (Alter)	じゅっさい[<br>jussai]<br><br>じっさい[<br>jissai]		Alter
OK.L,m3ylE	Japanese Urania	vhs-fr::Zähleinheitensuffixe	なんさい	? (Alter)	なんさい[<br>nansai]		Alter
ulWNF&>(CI	Japanese Urania	vhs-fr::Zähleinheitensuffixe	いっさつ	1 (Bücher & Hefte)	いっさつ[<br>issatsu]		Bücher::und::Hefte
xK8wC:)~Q-	Japanese Urania	vhs-fr::Zähleinheitensuffixe	にさつ	2 (Bücher & Hefte)	にさつ[<br>nisatsu]		Bücher::und::Hefte
uLrVLmnHYa	Japanese Urania	vhs-fr::Zähleinheitensuffixe	さんさつ	3 (Bücher & Hefte)	さんさつ[<br>sansatsu]		Bücher::und::Hefte
l{0E!1XP-8	Japanese Urania	vhs-fr::Zähleinheitensuffixe	よんさつ	4 (Bücher & Hefte)	よんさつ[<br>yonsatsu]		Bücher::und::Hefte
h%p>>X]gg^	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ごさつ	5 (Bücher & Hefte)	ごさつ[<br>gosatsu]		Bücher::und::Hefte
Bz)3ynq8if	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ろくさつ	6 (Bücher & Hefte)	ろくさつ[<br>rokusatsu]		Bücher::und::Hefte
o%YnM&UHS|	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ななさつ	7 (Bücher & Hefte)	ななさつ[<br>nanasatsu]		Bücher::und::Hefte
cm1DttR*f8	Japanese Urania	vhs-fr::Zähleinheitensuffixe	はっさつ	8 (Bücher & Hefte)	はっさつ[<br>hassatsu]		Bücher::und::Hefte
d$iPsmh9.1	Japanese Urania	vhs-fr::Zähleinheitensuffixe	きゅうさつ	9 (Bücher & Hefte)	きゅうさつ[<br>kyūsatsu]		Bücher::und::Hefte
lgD_`6493j	Japanese Urania	vhs-fr::Zähleinheitensuffixe	じゅっさつ<br>じっさつ	10 (Bücher & Hefte)	じゅっさつ[<br>jussatsu]<br>じっさつ[<br>jissatsu]		Bücher::und::Hefte
r2Td3zQ}di	Japanese Urania	vhs-fr::Zähleinheitensuffixe	なんさつ	? (Bücher & Hefte)	なんさつ[<br>nansatsu]		Bücher::und::Hefte
e>H}nZ8Ql^	Japanese Urania	vhs-fr::Zähleinheitensuffixe	いちまい	1 (Dünne, flache Gegenstände)	いちまい[<br>ichimai]		Dünne::flache::Gegenstände
Gmu[r?%$JC	Japanese Urania	vhs-fr::Zähleinheitensuffixe	にまい	2 (Dünne, flache Gegenstände)	にまい[<br>nimai]		Dünne::flache::Gegenstände
GQi&{,9G_e	Japanese Urania	vhs-fr::Zähleinheitensuffixe	さんまい	3 (Dünne, flache Gegenstände)	さんまい[<br>sanmai]		Dünne::flache::Gegenstände
lwBF2sVcmz	Japanese Urania	vhs-fr::Zähleinheitensuffixe	よんまい	4 (Dünne, flache Gegenstände)	よんまい[<br>yonmai]		Dünne::flache::Gegenstände
oO0xSu,1@L	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ごまい	5 (Dünne, flache Gegenstände)	ごまい[<br>gomai]		Dünne::flache::Gegenstände
F|4ty}7^c_	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ろくまい	6 (Dünne, flache Gegenstände)	ろくまい[<br>rokumai]		Dünne::flache::Gegenstände
dyJuxOhq$G	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ななまい	7 (Dünne, flache Gegenstände)	ななまい[<br>nanamai]		Dünne::flache::Gegenstände
Efunu*Ww2=	Japanese Urania	vhs-fr::Zähleinheitensuffixe	はちまい	8 (Dünne, flache Gegenstände)	はちまい[<br>hachimai]		Dünne::flache::Gegenstände
w7[`$|pVG0	Japanese Urania	vhs-fr::Zähleinheitensuffixe	きゅうまい	9 (Dünne, flache Gegenstände)	きゅうまい[<br>kyūmai]		Dünne::flache::Gegenstände
"z`htnd,#v1"	Japanese Urania	vhs-fr::Zähleinheitensuffixe	じゅまい	10 (Dünne, flache Gegenstände)	じゅまい[<br>jūmai]		Dünne::flache::Gegenstände
GlQ2.$mbjK	Japanese Urania	vhs-fr::Zähleinheitensuffixe	なんまい	? (Dünne, flache Gegenstände)	なんまい[<br>nanmai]		Dünne::flache::Gegenstände
"N(S[#:pe`%"	Japanese Urania	vhs-fr::Zähleinheitensuffixe	いっぽん	1 (Dünne, lange Gegenstände)	いっぽん[<br>ippon]		Dünne::lange::Gegenstände
on|i&c,E,.	Japanese Urania	vhs-fr::Zähleinheitensuffixe	にほん	2 (Dünne, lange Gegenstände)	にほん[<br>nihon]		Dünne::lange::Gegenstände
y&N4hnhS&O	Japanese Urania	vhs-fr::Zähleinheitensuffixe	さんぼん	3 (Dünne, lange Gegenstände)	さんぼん[<br>sanbon]		Dünne::lange::Gegenstände
ju<H{y>9oB	Japanese Urania	vhs-fr::Zähleinheitensuffixe	よんほん	4 (Dünne, lange Gegenstände)	よんほん[<br>yonhon]		Dünne::lange::Gegenstände
PIyOCXrQ;(	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ごほん	5 (Dünne, lange Gegenstände)	ごほん[<br>gohon]		Dünne::lange::Gegenstände
uAxa2&|*fC	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ろっぽん	6 (Dünne, lange Gegenstände)	ろっぽん[<br>roppon]		Dünne::lange::Gegenstände
GgbW!6_rt;	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ななほん	7 (Dünne, lange Gegenstände)	ななほん[<br>nanahon]		Dünne::lange::Gegenstände
rx~MZ1H1e<	Japanese Urania	vhs-fr::Zähleinheitensuffixe	はっぽん	8 (Dünne, lange Gegenstände)	はっぽん[<br>happon]		Dünne::lange::Gegenstände
MLsWzdR.%T	Japanese Urania	vhs-fr::Zähleinheitensuffixe	きゅうほん	9 (Dünne, lange Gegenstände)	きゅうほん[<br>kyūhon]		Dünne::lange::Gegenstände
vgA)V:njC-	Japanese Urania	vhs-fr::Zähleinheitensuffixe	じゅっぽん<br>じっぽん	10 (Dünne, lange Gegenstände)	じゅっぽん[<br>juppon]<br>じっぽん[<br>jippon]		Dünne::lange::Gegenstände
"HF`#nB-V6u"	Japanese Urania	vhs-fr::Zähleinheitensuffixe	なんぼん	? (Dünne, lange Gegenstände)	なんぼん[<br>nanbon]		Dünne::lange::Gegenstände
r.~&UiS8}n	Japanese Urania	vhs-fr::Zähleinheitensuffixe	じゅっぱい<br>じっぱい	10 (Getränke in Tassen und Gläsern)	じゅっぱい[<br>juppai]<br>じっぱい[<br>jippai]		Getränke::in::Tassen::und::Gläsern
yj.q^IN0%	Japanese Urania	vhs-fr::Zähleinheitensuffixe	なんばい	? (Getränke in Tassen und Gläsern)	なんばい[<br>nanbai]		Getränke::in::Tassen::und::Gläsern
eB,CumMN;W	Japanese Urania	vhs-fr::Zähleinheitensuffixe	いっかい	1 (Häufigkeit)	いっかい[<br>ikkai]		Häufigkeit
o9oGOIgNeF	Japanese Urania	vhs-fr::Zähleinheitensuffixe	にかい	2 (Häufigkeit)	にかい[<br>nikai]		Häufigkeit
p/<z>9[ROU	Japanese Urania	vhs-fr::Zähleinheitensuffixe	さんかい	3 (Häufigkeit)	さんかい[<br>sankai]		Häufigkeit
rd)o^jAtu(	Japanese Urania	vhs-fr::Zähleinheitensuffixe	よんかい	4 (Häufigkeit)	よんかい[<br>yonkai]		Häufigkeit
z!_k5OswN^	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ごかい	5 (Häufigkeit)	ごかい[<br>gokai]		Häufigkeit
r4Xb(Ssf}3	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ろっかい	6 (Häufigkeit)	ろっかい[<br>rokkai]		Häufigkeit
E;WZ[-r[,	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ななかい	7 (Häufigkeit)	ななかい[<br>nanakai]		Häufigkeit
lB1|0sFFah	Japanese Urania	vhs-fr::Zähleinheitensuffixe	はっかい	8 (Häufigkeit)	はっかい[<br>hakkai]		Häufigkeit
dMI<5fJ%H:	Japanese Urania	vhs-fr::Zähleinheitensuffixe	きゅうかい	9 (Häufigkeit)	きゅうかい[<br>kyūkai]		Häufigkeit
f,*<n4ol+d	Japanese Urania	vhs-fr::Zähleinheitensuffixe	じゅっかい<br>じっかい	10 (Häufigkeit)	じゅっかい[<br>jukkai]<br>じっかい[<br>jikkai]		Häufigkeit
"kx#nd)IR]W"	Japanese Urania	vhs-fr::Zähleinheitensuffixe	なんかい	? (Häufigkeit)	なんかい[<br>nankai]		Häufigkeit
r7{^=|dE8D	Japanese Urania	vhs-fr::Zähleinheitensuffixe	いっけん	1 (Häuser)	いっけん[<br>ikken]		Häuser
bEt8%Nq)h2	Japanese Urania	vhs-fr::Zähleinheitensuffixe	にけん	2 (Häuser)	にけん[<br>niken]		Häuser
g>ZZdm{glx	Japanese Urania	vhs-fr::Zähleinheitensuffixe	さんげん	3 (Häuser)	さんげん[<br>sangen]		Häuser
p9rs+MWq*4	Japanese Urania	vhs-fr::Zähleinheitensuffixe	よんけん	4 (Häuser)	よんけん[<br>yonken]		Häuser
uf&TK5ri!D	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ごけん	5 (Häuser)	ごけん[<br>goken]		Häuser
Nx_DjCQ:.m	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ろっけん	6 (Häuser)	ろっけん[<br>rokken]		Häuser
bYVWFL7%gp	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ななけん	7 (Häuser)	ななけん[<br>nanaken]		Häuser
v<)no<N>oH	Japanese Urania	vhs-fr::Zähleinheitensuffixe	はっけん	8 (Häuser)	はっけん[<br>hakken]		Häuser
PMtFDxcR3/	Japanese Urania	vhs-fr::Zähleinheitensuffixe	きゅうけん	9 (Häuser)	きゅうけん[<br>kyūken]		Häuser
c:A67xTAf{	Japanese Urania	vhs-fr::Zähleinheitensuffixe	じゅっけん<br>じっけん	10 (Häuser)	じゅっけん[<br>jukken]<br>じっけん[<br>jikken]		Häuser
sHA+I0OIB[	Japanese Urania	vhs-fr::Zähleinheitensuffixe	なんげん	? (Häuser)	なんげん[<br>nangen]		Häuser
I;Y(X_U;U/	Japanese Urania	vhs-fr::Zähleinheitensuffixe	いっこ	1 (Kleine Gegenstände)	いっこ[<br>ikko]		Kleine::Gegenstände
P(4VI8l)aM	Japanese Urania	vhs-fr::Zähleinheitensuffixe	にこ	2 (Kleine Gegenstände)	にこ[<br>niko]		Kleine::Gegenstände
zLYv-T@A@`	Japanese Urania	vhs-fr::Zähleinheitensuffixe	さんこ	3 (Kleine Gegenstände)	さんこ[<br>sanko]		Kleine::Gegenstände
i]i5a;=wBM	Japanese Urania	vhs-fr::Zähleinheitensuffixe	よんこ	4 (Kleine Gegenstände)	よんこ[<br>yonko]		Kleine::Gegenstände
t<//8sk.xU	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ごこ	5 (Kleine Gegenstände)	ごこ[<br>goko]		Kleine::Gegenstände
GV7(GV_L)q	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ろっこ	6 (Kleine Gegenstände)	ろっこ[<br>rokko]		Kleine::Gegenstände
h;01SgR&N4	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ななこ	7 (Kleine Gegenstände)	ななこ[<br>nanako]		Kleine::Gegenstände
v+s|^]h!3W	Japanese Urania	vhs-fr::Zähleinheitensuffixe	はっこ	8 (Kleine Gegenstände)	はっこ[<br>hakko]		Kleine::Gegenstände
"xe[#:-foJv"	Japanese Urania	vhs-fr::Zähleinheitensuffixe	きゅうこ	9 (Kleine Gegenstände)	きゅうこ[<br>kyūko]		Kleine::Gegenstände
e[`xs1EUKA	Japanese Urania	vhs-fr::Zähleinheitensuffixe	じゅっこ<br>じっこ	10 (Kleine Gegenstände)	じゅっこ[<br>jukko]<br>じっこ[<br>jikko]		Kleine::Gegenstände
LEiN,E5CC7	Japanese Urania	vhs-fr::Zähleinheitensuffixe	なんこ	? (Kleine Gegenstände)	なんこ[<br>nanko]		Kleine::Gegenstände
pcI^-6VH9F	Japanese Urania	vhs-fr::Zähleinheitensuffixe	いちだい	1 (Maschinen & Fahrzeuge)	いちだい[<br>ichidai]		Maschinen::und::Fahrzeuge
O6e]a*3|Q7	Japanese Urania	vhs-fr::Zähleinheitensuffixe	にだい	2 (Maschinen & Fahrzeuge)	にだい[<br>nidai]		Maschinen::und::Fahrzeuge
Ac7bL!EdN0	Japanese Urania	vhs-fr::Zähleinheitensuffixe	さんだい	3 (Maschinen & Fahrzeuge)	さんだい[<br>sandai]		Maschinen::und::Fahrzeuge
uleK&y_~Pn	Japanese Urania	vhs-fr::Zähleinheitensuffixe	よんだい	4 (Maschinen & Fahrzeuge)	よんだい[<br>yondai]		Maschinen::und::Fahrzeuge
ymEaVSX*Vg	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ごだい	5 (Maschinen & Fahrzeuge)	ごだい[<br>godai]		Maschinen::und::Fahrzeuge
"K~/r)#}y)/"	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ろくだい	6 (Maschinen & Fahrzeuge)	ろくだい[<br>rokudai]		Maschinen::und::Fahrzeuge
tipd~e4j)g	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ななだい	7 (Maschinen & Fahrzeuge)	ななだい[<br>nanadai]		Maschinen::und::Fahrzeuge
N4L(D(oT.$	Japanese Urania	vhs-fr::Zähleinheitensuffixe	はちだい	8 (Maschinen & Fahrzeuge)	はちだい[<br>hachidai]		Maschinen::und::Fahrzeuge
F^.kp8F=?9	Japanese Urania	vhs-fr::Zähleinheitensuffixe	きゅうだい	9 (Maschinen & Fahrzeuge)	きゅうだい[<br>kyūdai]		Maschinen::und::Fahrzeuge
Ffdb9J~9zx	Japanese Urania	vhs-fr::Zähleinheitensuffixe	じゅだい	10 (Maschinen & Fahrzeuge)	じゅだい[<br>jūdai]		Maschinen::und::Fahrzeuge
c<jt)y`>_X	Japanese Urania	vhs-fr::Zähleinheitensuffixe	なんだい	? (Maschinen & Fahrzeuge)	なんだい[<br>nandai]		Maschinen::und::Fahrzeuge
d7,{<`3s(n	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ひとり	1 (Personen)	ひとり[<br>hitori]		Personen
zXn<J:hs(D	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ふたり	2 (Personen)	ふたり[<br>futari]		Personen
jcJUw.K9&,	Japanese Urania	vhs-fr::Zähleinheitensuffixe	さんにん	3 (Personen)	さんにん[<br>sannin]		Personen
O=gp-*B5d8	Japanese Urania	vhs-fr::Zähleinheitensuffixe	よにん	4 (Personen)	よにん[<br>yonin]		Personen
vI(0$&{/s4	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ごにん	5 (Personen)	ごにん[<br>gonin]		Personen
Av9@}=AE)J	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ろくにん	6 (Personen)	ろくにん[<br>rokunin]		Personen
"Bjdr#bX%2:"	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ななにん<br>しちにん	7 (Personen)	ななにん[<br>nananin]<br>しちにん[<br>shichinin]		Personen
yg`YSY*yW`	Japanese Urania	vhs-fr::Zähleinheitensuffixe	はちにん	8 (Personen)	はちにん[<br>hachinin]		Personen
wN9W!y=&+B	Japanese Urania	vhs-fr::Zähleinheitensuffixe	きゅうにん	9 (Personen)	きゅうにん[<br>kyūnin]		Personen
DC`~<,F3C*	Japanese Urania	vhs-fr::Zähleinheitensuffixe	じゅにん	10 (Personen)	じゅにん[<br>jūnin]		Personen
gLEsnR.6WE	Japanese Urania	vhs-fr::Zähleinheitensuffixe	なんにん	? (Personen)	なんにん[<br>nannin]		Personen
g@It;MkpiT	Japanese Urania	vhs-fr::Zähleinheitensuffixe	いちばん	1 (Reihenfolge)	いちばん[<br>ichiban]		Reihenfolge
c}:qI`^X(+	Japanese Urania	vhs-fr::Zähleinheitensuffixe	にばん	2 (Reihenfolge)	にばん[<br>niban]		Reihenfolge
Jg{)$2j-{q	Japanese Urania	vhs-fr::Zähleinheitensuffixe	さんばん	3 (Reihenfolge)	さんばん[<br>sanban]		Reihenfolge
rczf6%PBOq	Japanese Urania	vhs-fr::Zähleinheitensuffixe	よんばん	4 (Reihenfolge)	よんばん[<br>yonban]		Reihenfolge
cI-vv%wV{u	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ごばん	5 (Reihenfolge)	ごばん[<br>goban]		Reihenfolge
oV{8&>-I&4	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ろくばん	6 (Reihenfolge)	ろくばん[<br>rokuban]		Reihenfolge
EC-eB]hS`*	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ななばん	7 (Reihenfolge)	ななばん[<br>nanaban]		Reihenfolge
q_}+S.O^wm	Japanese Urania	vhs-fr::Zähleinheitensuffixe	はちばん	8 (Reihenfolge)	はちばん[<br>hachiban]		Reihenfolge
L.]%Or~][/	Japanese Urania	vhs-fr::Zähleinheitensuffixe	きゅうばん	9 (Reihenfolge)	きゅうばん[<br>kyūban]		Reihenfolge
fpZi?k+;MF	Japanese Urania	vhs-fr::Zähleinheitensuffixe	じゅばん	10 (Reihenfolge)	じゅばん[<br>jūban]		Reihenfolge
ra66f.h(+Y	Japanese Urania	vhs-fr::Zähleinheitensuffixe	なんばん	? (Reihenfolge)	なんばん[<br>nanban]		Reihenfolge
DLom5w-`->	Japanese Urania	vhs-fr::Zähleinheitensuffixe	いっそく	1 (Schuhe & Socken)	いっそく[<br>issoku]		Schuhe::und::Socken
hNEMjT,u3s	Japanese Urania	vhs-fr::Zähleinheitensuffixe	にそく	2 (Schuhe & Socken)	にそく[<br>nisoku]		Schuhe::und::Socken
u%&L%g[ZcE	Japanese Urania	vhs-fr::Zähleinheitensuffixe	さんぞく	3 (Schuhe & Socken)	さんぞく[<br>sanzoku]		Schuhe::und::Socken
oqZ6U[l)tt	Japanese Urania	vhs-fr::Zähleinheitensuffixe	よんそく	4 (Schuhe & Socken)	よんそく[<br>yonsoku]		Schuhe::und::Socken
lmTyXV5XOg	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ごそく	5 (Schuhe & Socken)	ごそく[<br>gosoku]		Schuhe::und::Socken
o1J+>(`[_&	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ろくそく	6 (Schuhe & Socken)	ろくそく[<br>rokusoku]		Schuhe::und::Socken
N{&!!&g$xK	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ななそく	7 (Schuhe & Socken)	ななそく[<br>nanasoku]		Schuhe::und::Socken
c|MJW=&Tz~	Japanese Urania	vhs-fr::Zähleinheitensuffixe	はっそく	8 (Schuhe & Socken)	はっそく[<br>hassoku]		Schuhe::und::Socken
dI7)}@1-LV	Japanese Urania	vhs-fr::Zähleinheitensuffixe	きゅうそく	9 (Schuhe & Socken)	きゅうそく[<br>kyūsoku]		Schuhe::und::Socken
y5l&w`8*?l	Japanese Urania	vhs-fr::Zähleinheitensuffixe	じゅっそく<br>じっそく	10 (Schuhe & Socken)	じゅっそく[<br>jussoku]<br>じっそく[<br>jissoku]		Schuhe::und::Socken
xQa?0uAXJb	Japanese Urania	vhs-fr::Zähleinheitensuffixe	なんぞく	? (Schuhe & Socken)	なんぞく[<br>nanzoku]		Schuhe::und::Socken
u`kKFU}Ld[	Japanese Urania	vhs-fr::Zähleinheitensuffixe	いっかい	1 (Stockwerke)	いっかい[<br>ikkai]		Stockwerke
"i;W90%`#c@"	Japanese Urania	vhs-fr::Zähleinheitensuffixe	にかい	2 (Stockwerke)	にかい[<br>nikai]		Stockwerke
f+8ljqWGi]	Japanese Urania	vhs-fr::Zähleinheitensuffixe	さんがい	3 (Stockwerke)	さんがい[<br>sangai]		Stockwerke
nLJ1Q8iO`3	Japanese Urania	vhs-fr::Zähleinheitensuffixe	よんかい	4 (Stockwerke)	よんかい[<br>yonkai]		Stockwerke
v7(QH7{Z;!	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ごかい	5 (Stockwerke)	ごかい[<br>gokai]		Stockwerke
Pq+;(tQkx8	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ろっかい	6 (Stockwerke)	ろっかい[<br>rokkai]		Stockwerke
ITprXCp}!|	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ななかい	7 (Stockwerke)	ななかい[<br>nanakai]		Stockwerke
"bLR#6&<`m:"	Japanese Urania	vhs-fr::Zähleinheitensuffixe	はっかい	8 (Stockwerke)	はっかい[<br>hakkai]		Stockwerke
si+UR%{b(.	Japanese Urania	vhs-fr::Zähleinheitensuffixe	きゅうかい	9 (Stockwerke)	きゅうかい[<br>kyūkai]		Stockwerke
nmPBwlYw%%	Japanese Urania	vhs-fr::Zähleinheitensuffixe	じゅっかい<br>じっかい	10 (Stockwerke)	じゅっかい[<br>jukkai]<br>じっかい[<br>jikkai]		Stockwerke
iE&b|j8H}^	Japanese Urania	vhs-fr::Zähleinheitensuffixe	なんがい	? (Stockwerke)	なんがい[<br>nangai]		Stockwerke
O-70QgdeGv	Japanese Urania	vhs-fr::Zähleinheitensuffixe	いっぴき	1 (Kleine Tiere, Insekten und Fische)	いっぴき[<br>ippiki]		Kleine::Tiere::Insekten::und::Fische
t&;TyJWdW^	Japanese Urania	vhs-fr::Zähleinheitensuffixe	にひき	2 (Kleine Tiere, Insekten und Fische)	にひき[<br>nihiki]		Kleine::Tiere::Insekten::und::Fische
H=@9u^qen(	Japanese Urania	vhs-fr::Zähleinheitensuffixe	さんびき	3 (Kleine Tiere, Insekten und Fische)	さんびき[<br>sanbiki]		Kleine::Tiere::Insekten::und::Fische
g0.A9<zh-.	Japanese Urania	vhs-fr::Zähleinheitensuffixe	よんひき	4 (Kleine Tiere, Insekten und Fische)	よんひき[<br>yonhiki]		Kleine::Tiere::Insekten::und::Fische
dVcZOu.V4F	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ごひき	5 (Kleine Tiere, Insekten und Fische)	ごひき[<br>gohiki]		Kleine::Tiere::Insekten::und::Fische
MuZ,.M`e??	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ろっぴき	6 (Kleine Tiere, Insekten und Fische)	ろっぴき[<br>roppiki]		Kleine::Tiere::Insekten::und::Fische
Ow%5>QS8jJ	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ななひき	7 (Kleine Tiere, Insekten und Fische)	ななひき[<br>nanahiki]		Kleine::Tiere::Insekten::und::Fische
u+4hGe31Lo	Japanese Urania	vhs-fr::Zähleinheitensuffixe	はっぴき	8 (Kleine Tiere, Insekten und Fische)	はっぴき[<br>happiki]		Kleine::Tiere::Insekten::und::Fische
iDv|*Md^Gc	Japanese Urania	vhs-fr::Zähleinheitensuffixe	きゅうひき	9 (Kleine Tiere, Insekten und Fische)	きゅうひき[<br>kyūhiki]		Kleine::Tiere::Insekten::und::Fische
kDic7+wW*b	Japanese Urania	vhs-fr::Zähleinheitensuffixe	じゅっぴき<br>じっぴき	10 (Kleine Tiere, Insekten und Fische)	じゅっぴき[<br>juppiki]<br>じっぴき[<br>jippiki]		Kleine::Tiere::Insekten::und::Fische
"p.Hf5MA#!<"	Japanese Urania	vhs-fr::Zähleinheitensuffixe	なんびき	? (Kleine Tiere, Insekten und Fische)	なんびき[<br>nanbiki]		Kleine::Tiere::Insekten::und::Fische
lHNNhq8rY6	Japanese Urania	vhs-fr::Zähleinheitensuffixe	いっちゃく	1 (Kleidung)	いっちゃく[<br>icchaku]		Kleidung
pY:DRjC?R`	Japanese Urania	vhs-fr::Zähleinheitensuffixe	にちゃく	2 (Kleidung)	にちゃく[<br>nichaku]		Kleidung
BW}HJ8lOst	Japanese Urania	vhs-fr::Zähleinheitensuffixe	さんちゃく	3 (Kleidung)	さんちゃく[<br>sanchaku]		Kleidung
FU>w<M$<(h	Japanese Urania	vhs-fr::Zähleinheitensuffixe	よんちゃく	4 (Kleidung)	よんちゃく[<br>yonchaku]		Kleidung
r:g:$GrCV/	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ごちゃく	5 (Kleidung)	ごちゃく[<br>gochaku]		Kleidung
e3X2K9:C*F	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ろくちゃく	6 (Kleidung)	ろくちゃく[<br>rokuchaku]		Kleidung
"BY&0k#zh,-"	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ななちゃく	7 (Kleidung)	ななちゃく[<br>nanachaku]		Kleidung
Q(S8R+XT]^	Japanese Urania	vhs-fr::Zähleinheitensuffixe	はっちゃく	8 (Kleidung)	はっちゃく[<br>hacchaku]		Kleidung
"mNi#J$Ya!n"	Japanese Urania	vhs-fr::Zähleinheitensuffixe	きゅうちゃく	9 (Kleidung)	きゅうちゃく[<br>kyūchaku]		Kleidung
qEi$xOE!&/	Japanese Urania	vhs-fr::Zähleinheitensuffixe	じゅっちゃく<br>じっちゃく	10 (Kleidung)	じゅっちゃく[<br>jucchaku]<br>じっちゃく[<br>jicchaku]		Kleidung
L9)<vn=dqs	Japanese Urania	vhs-fr::Zähleinheitensuffixe	なんちゃく	? (Kleidung)	なんちゃく[<br>nanchaku]		Kleidung
K_B.M@i.>i	Japanese Urania	vhs-fr::Zähleinheitensuffixe	いっぱい	1 (Getränke in Tassen und Gläsern)	いっぱい[<br>ippai]		Getränke::in::Tassen::und::Gläsern
y;u2+A:xW@	Japanese Urania	vhs-fr::Zähleinheitensuffixe	にはい	2 (Getränke in Tassen und Gläsern)	にはい[<br>nihai]		Getränke::in::Tassen::und::Gläsern
v=}revRmz$	Japanese Urania	vhs-fr::Zähleinheitensuffixe	さんばい	3 (Getränke in Tassen und Gläsern)	さんばい[<br>sanbai]		Getränke::in::Tassen::und::Gläsern
dzTRAu25v}	Japanese Urania	vhs-fr::Zähleinheitensuffixe	よんはい	4 (Getränke in Tassen und Gläsern)	よんはい[<br>yonhai]		Getränke::in::Tassen::und::Gläsern
y@y?N/Is:e	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ごはい	5 (Getränke in Tassen und Gläsern)	ごはい[<br>gohai]		Getränke::in::Tassen::und::Gläsern
ypDneomBmv	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ろっぱい	6 (Getränke in Tassen und Gläsern)	ろっぱい[<br>roppai]		Getränke::in::Tassen::und::Gläsern
mh__qpOKX0	Japanese Urania	vhs-fr::Zähleinheitensuffixe	ななはい	7 (Getränke in Tassen und Gläsern)	ななはい[<br>nanahai]		Getränke::in::Tassen::und::Gläsern
mo|SMLT2+/	Japanese Urania	vhs-fr::Zähleinheitensuffixe	はっぱい	8 (Getränke in Tassen und Gläsern)	はっぱい[<br>happai]		Getränke::in::Tassen::und::Gläsern
M05vZr}4-{	Japanese Urania	vhs-fr::Zähleinheitensuffixe	きゅうはい	9 (Getränke in Tassen und Gläsern)	きゅうはい[<br>kyūhai]		Getränke::in::Tassen::und::Gläsern
