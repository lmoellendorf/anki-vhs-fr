/*
 * Use nested markup for double-sided ruby positioning
 * https://w3c.github.io/i18n-drafts/articles/ruby/styling.en#double_position
 */
function setupCard() {
	var newDiv;
	var parentRuby;
	var rubyDiv;

	/* Create a new div to hold the transformed structure */
	newDiv = document.createElement('div');
	/* Create a new div to hold the next ruby */
	rubyDiv = document.createElement('div');
	//alert( rd.innerText );
	//alert( rd.innerHTML );

	/* Iterate through all elements to transform the structure */
	for (var child = rd.firstChild; child; child = child.nextSibling) {
		//alert( child.innerHTML + '<br>' + child.innerText  + '<br>' + child.textContent);
		if (child.nodeType != Node.TEXT_NODE) {
			//alert( child.textContent );
			/* Add parent ruby element for nested markup */
			rubyDiv.appendChild(document.createElement('ruby'));
			parentRuby = rubyDiv.children[0];
			if(child.tagName === "RUBY") {
				/* get rb element */
				var rb = child.querySelector('rb');

				if(rb) {
					//alert( rb.innerHTML );
					/* Select the rt elements */
					var rtElements = child.querySelectorAll('rt');
					/* Create a new ruby element for the rb text */
					var newRuby = document.createElement('ruby');
					newRuby.appendChild(document.createTextNode(rb.textContent));
					/* Append the new ruby to the parent ruby */
					parentRuby.appendChild(newRuby);

					/* Append rt elements to the new ruby */
					rtElements.forEach((rt) => {
						//alert( rt.innerHTML );
						/* Split rt text at <br> */
						var textWithLineBreaks = rt.innerHTML.replace(/<br\s*\/?>/gi, '\n');
						var arr = textWithLineBreaks.split('\n');
						/* Create new rt element for hiragana furigana */
						var newRt = document.createElement('rt');
						newRt.appendChild(document.createTextNode(arr[0]));
						/* Append the new furigana to the new ruby */
						newRuby.appendChild(newRt);

						/* Is there is a second furigana (after <br>)? */
						if (arr[1]) {
							/* Create a new rt for the romaji furigana */
							var newRt = document.createElement('rt');
							newRt.appendChild(document.createTextNode(arr[1]));
							/* Append this directly to the parent ruby */
							/* (for nested markup) */
							parentRuby.appendChild(newRt);
							/* Append a rb element (for nested markup) */
							parentRuby.appendChild(document.createElement('rb'));
						}
					});
					/* append the ruby structure */
					newDiv.appendChild(rubyDiv);
				}
			}
			if(child.tagName === "BR") {
					rubyDiv = document.createElement('div');
					/* Add parent ruby element for nested markup */
					rubyDiv.appendChild(document.createElement('ruby'));
					parentRuby = rubyDiv.children[0];
			}
		} else {
			//alert( child.textContent );
			/* append the text node */
			rubyDiv.appendChild(document.createTextNode(child.textContent));
			newDiv.appendChild(rubyDiv);
			rubyDiv = document.createElement('div');
		}
	};

	/* Replace jp div content by the transformed HTML */
	rd.innerHTML = newDiv.innerHTML;
	//alert( newDiv.innerHTML );
}


