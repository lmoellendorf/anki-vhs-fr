" add this to your ~/.vimrc to source this file:
"autocmd BufRead,BufNewFile /path/to/anki/vhs-fr/* {
"  if filereadable('.vimrc')
"    execute 'source .vimrc'
"  endif
"}
" set filetype to CSV - autocmd does not work if this file itself is sourced
" using autocmd
"autocmd BufNewFile,BufRead vhs-fr__*.txt set filetype=csv
if expand("%:p") =~ 'vhs-fr__.*\.txt$' | set ft=csv | endif
" the key mappings below use https://github.com/PSeitz/wana_kana_rust
" to install:
" cargo install wana_kana
"
" map to romaji conversion of current word to [r
if expand("%:p") =~ 'vhs-fr__.*\.txt$' | nmap [r yiwhe:exe 'norm a[<br>'.  system("to_romaji ". getreg(''))[:-2] . ']'<cr> | endif
" type hiragana then type [f => it will prepend it with '[' and append '<br>' followed by romanji and ']'
if expand("%:p") =~ 'vhs-fr__.*\.txt$' | nmap [f lmfybi[<esc>`f:exe 'norm a<br>'. system("to_romaji ". getreg(''))[:-2] . ']'<cr> | endif
" For the record
" convert content of unnamed register to kana and assign to named register k
" let @k = system("to_kana ". getreg(''))
" convert content of unnamed register to romaji and assign to named register r
" let @r = system("to_romaji ". getreg(''))
" convert content of unnamed register `getreg('')` to romaji, remove trailing newline `[:-2]` and append it to current position
" exe 'norm a'.  system("to_romaji ". getreg(''))[:-2]
" map to katana conversion of current word to [r
" nmap [k yiwe:exe 'norm a'.  system("to_kana ". getreg(''))[:-2]<cr>
" show tabs as characters
if expand("%:p") =~ 'vhs-fr__.*\.txt$' | setlocal list | endif
if expand("%:p") =~ 'vhs-fr__.*\.txt$' | setlocal listchars=tab:>- | endif
if expand("%:p") =~ 'vhs-fr__.*\.txt$' | setlocal sidescroll=1 | endif
if expand("%:p") =~ 'vhs-fr__.*\.txt$' | setlocal tabstop=64 | endif
