#!/usr/bin/env python3
import anki
from anki.collection import Collection
from anki.collection import ImportCsvRequest

col = Collection("/home/lars/.local/share/Anki2/Benutzer 1/collection.anki2")

def importFile(file):
    metadata = col.get_csv_metadata(path=file, delimiter=None)
    request = ImportCsvRequest(path=file, metadata=metadata)
    response = col.import_csv(request)
    print(
    "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    )
    print(file)
    print(response.log.found_notes, list(response.log.updated), list(response.log.new))
    print(
    "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
    )


"""
id: 1728164348096
name: "vhs-fr::Adjektive"
"""
file="vhs-fr__Adjektive.txt"
importFile(file)

"""
id: 1708204523972
name: "vhs-fr::Alltagsgegenstände"
"""
file="vhs-fr__Alltagsgegenstände.txt"
importFile(file)

"""
id: 1728241905067
name: "vhs-fr::Einfache Sätze"
"""
file="vhs-fr__Einfache Sätze.txt"
importFile(file)

"""
id: 1703635275134
name: "vhs-fr::Farben"
"""
file="vhs-fr__Farben.txt"
importFile(file)

"""
id: 1711266023492
name: "vhs-fr::Flexion der Verben"
"""
file="vhs-fr__Flexion der Verben.txt"
importFile(file)

"""
id: 1703635405245
name: "vhs-fr::Geschmack"
"""
file="vhs-fr__Geschmack.txt"
importFile(file)

"""
id: 1704744504758
name: "vhs-fr::Japanisch im Sauseschritt"
"""
file="vhs-fr__Japanisch im Sauseschritt.txt"
importFile(file)

"""
id: 1708204580026
name: "vhs-fr::Zeitangaben"
"""
file="vhs-fr__Zeitangaben.txt"
importFile(file)

"""
id: 1703958168032
name: "vhs-fr::Zähleinheitensuffixe"
"""
file="vhs-fr__Zähleinheitensuffixe.txt"
importFile(file)

