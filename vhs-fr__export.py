#!/usr/bin/env python3
import anki
from anki.collection import Collection
from anki.collection import DeckIdLimit

def unleech(file):
    f = open(file,'r')
    a = ['leech ']
    lst = []
    for line in f:
        for word in a:
            if word in line:
                line = line.replace(word,'')
        lst.append(line)
    f.close()
    f = open(file,'w')
    for line in lst:
        f.write(line)
    f.close()

col = Collection("/home/lars/.local/share/Anki2/Benutzer 1/collection.anki2")

"""
id: 1728164348096
name: "vhs-fr::Adjektive"
"""
file="vhs-fr__Adjektive.txt"
deck=DeckIdLimit(deck_id=1728164348096)
col.export_note_csv(out_path=file, limit=deck, with_html=True, with_tags=True, with_deck=True, with_notetype=True, with_guid=True)
unleech(file)

"""
id: 1708204523972
name: "vhs-fr::Alltagsgegenstände"
"""
file="vhs-fr__Alltagsgegenstände.txt"
deck=DeckIdLimit(deck_id=1708204523972)
col.export_note_csv(out_path=file, limit=deck, with_html=True, with_tags=True, with_deck=True, with_notetype=True, with_guid=True)
unleech(file)

"""
id: 1728241905067
name: "vhs-fr::Einfache Sätze"
"""
file="vhs-fr__Einfache Sätze.txt"
deck=DeckIdLimit(deck_id=1728241905067)
col.export_note_csv(out_path=file, limit=deck, with_html=True, with_tags=True, with_deck=True, with_notetype=True, with_guid=True )
unleech(file)

"""
id: 1703635275134
name: "vhs-fr::Farben"
"""
file="vhs-fr__Farben.txt"
deck=DeckIdLimit(deck_id=1703635275134)
col.export_note_csv(out_path=file, limit=deck, with_html=True, with_tags=True, with_deck=True, with_notetype=True, with_guid=True)
unleech(file)

"""
id: 1711266023492
name: "vhs-fr::Flexion der Verben"
"""
file="vhs-fr__Flexion der Verben.txt"
deck=DeckIdLimit(deck_id=1711266023492)
col.export_note_csv(out_path=file, limit=deck, with_html=True, with_tags=True, with_deck=True, with_notetype=True, with_guid=True)
unleech(file)

"""
id: 1703635405245
name: "vhs-fr::Geschmack"
"""
file="vhs-fr__Geschmack.txt"
deck=DeckIdLimit(deck_id=1703635405245)
col.export_note_csv(out_path=file, limit=deck, with_html=True, with_tags=True, with_deck=True, with_notetype=True, with_guid=True)
unleech(file)

"""
id: 1704744504758
name: "vhs-fr::Japanisch im Sauseschritt"
"""
file="vhs-fr__Japanisch im Sauseschritt.txt"
deck=DeckIdLimit(deck_id=1704744504758)
col.export_note_csv(out_path=file, limit=deck, with_html=True, with_tags=True, with_deck=True, with_notetype=True, with_guid=True)
unleech(file)

"""
id: 1708204580026
name: "vhs-fr::Zeitangaben"
"""
file="vhs-fr__Zeitangaben.txt"
deck=DeckIdLimit(deck_id=1708204580026)
col.export_note_csv(out_path=file, limit=deck, with_html=True, with_tags=True, with_deck=True, with_notetype=True, with_guid=True)
unleech(file)

"""
id: 1703958168032
name: "vhs-fr::Zähleinheitensuffixe"
"""
file="vhs-fr__Zähleinheitensuffixe.txt"
deck=DeckIdLimit(deck_id=1703958168032)
col.export_note_csv(out_path=file, limit=deck, with_html=True, with_tags=True, with_deck=True, with_notetype=True, with_guid=True)
unleech(file)
