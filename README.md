# Vokabeln zum Japanisch Kurs an der VHS Freiburg

Ich besuche den Japanisch Kurs an der Volkshochschule Freiburg im Breisgau und
veröffentliche hier meine Vokabel Sammlung für Anki.

[241410404 Japanisch - Zielniveau A 1 3. Semester](https://ankiweb.net/shared/info/712914300?cb=1708975654260)

Die Vokabeln "Japanisch im Sauseschritt" basieren auf den Anki Decks:

- [Japanisch im Sauseschritt](https://ankiweb.net/shared/info/1893369110) und
- [Japanisch im Sauseschritt (Romaji)](https://ankiweb.net/shared/info/2020573071)

Die anderen Vokabeln basieren auf Zusammenstellungen, die uns unsere Lehrerin
ausgehändigt hat.

Zu allen Kanji wird sowohl die Hiragana als auch die Romaji Schreibung
eingeblendet.

Zu allen Hiragana und Katakana wird die Romaji Schreibung eingeblendet:

- (w)o: das Zeichen を das als 'o' ausgesprochen wird
- pa-ti-: Katakana Dehnungen ー
- ē, ī, ō: Hiragana Dehnungen
- ん: ich habe versucht die Aussprache von ん jeweils durch das richtige 'n'
  oder 'm' darzustellen
- ten'in: Das ' ist als Trennung um zu sehen, es handelt sich um ein んい(ni)
  und nicht das に(ni)

# Was ist Anki?

[Anki](https://apps.ankiweb.net/) ist ein
[Open Source](https://de.wikipedia.org/wiki/Open_Source) Lernprogramm, dass auf
dem Karteikartenprinzip basiert.  Es ist für Windows, Mac, Linux, iPhone und
Android verfügbar.

![Screenshot von AnkiDroid](doc/images/Screenshot_AnkiDroid.gif){width=40%}

Der Vorteil gegenüber Karteikarten:

- Anki auf dem Smartphone ist immer dabei. D.h. wann immer sich die Gelegenheit
  ergibt, kann die Zeit mit Vokabellernen gefüllt werden.
- Die Decks lassen sich vervielfältigen und teilen.
- Wir können gemeinsam die Decks verbessern und erweitern.

# Wie funktioniert es?

(Getestet mit AnkiDroid ab Version 2.17.0)

- Installiere die App auf deinem Smartphone:
  - Android:
    - [AnkiDroid (Google Play)](https://play.google.com/store/apps/details?id=com.ichi2.anki)
    - alternativ: [AnkiDroid (F-Droid)](https://f-droid.org/en/packages/com.ichi2.anki/)
  - iPhone (kostenpflichtig):
    - [AnkiMobile Flashcards](https://apps.apple.com/us/app/ankimobile-flashcards/id373493387)
- [Lade die Vokabeln herunter](https://ankiweb.net/shared/info/712914300?cb=1708975654260)
- Installiere die Vokabeln per "Mit AnkiDroid öffnen"

Falls die oben genannten Apps für dich nicht in Frage kommen, kannst du auch
[AnkiWeb](https://ankiweb.net/decks) im Browser (Safari/Chrome/Firefox) nutzen.
Jedoch muss die Sammlung dann mit der App auf dem Computer
[synchronisiert](#anki-auf-dem-computer) werden.

# Und los!

Tippe auf einen der Stapel, um die Vokabelabfrage zu starten. du kannst auf
"vhs-fr" tippen, um aus allen Unterstapeln befragt zu werden, oder einen
Unterstapel antippen, um nur Vokabeln aus diesem zu lernen.

Die Abfrage erfolgt in beide Richtungen. Für jede Vokabel erst Japanisch ->
Deutsch, dann Deutsch -> Japanisch.

Mehr im [Anki 2.0 Handbuch (deutsch)](http://www.dennisproksch.de/anki) oder
im [Anki Manual](https://docs.ankiweb.net/).

# Fehler und Zusammenarbeit

Ich lerne selbst erst und mache Fehler. Daher liegt es in deiner eigenen
Verantwortung Fehler ggf. zu erkennen und zu beheben.

Wenn du möchtest, dass ich deine Korrekturen oder neuen Karten übernehme,
exportiere deinen Kartenstapel wie folgt:

![Stapel exportieren ](doc/images/anki_export_deck.png)
- Bitte die Stapel auf der zweiten Hierarchieebene exportieren.
  -  Also z.B. "vhs-fr" => "Japanisch im Sauseschritt"
  - und nicht "vhs-fr" => "Japanisch im Sauseschritt" => "Book1" oder "vhs-fr"

![Notizen als einfache Textdatei](doc/images/anki_export_as_textfile.png)
- [x] Einschließlich HTML und Verweise auf Mediendateien
- [x] Schlagwörter einschließen (aber **nicht** "leech")
- [x] Stapelnamen einbeziehen
- [x] Notiztypnamen einbeziehen
- Exportieren und entweder hier ein neues
  [Issue](https://gitlab.com/lmoellendorf/anki-vhs-fr/-/issues/new) anlegen und
  hochladen, oder einen Merge Request stellen.

# Anki auf dem Computer

Es kann nützlich sein die Sammlung auf Smartphone mit Anki auf deinem Computer
zu synchronisieren. Zum Beispiel, wenn du sie lieber auf dem Computer
bearbeiten möchtest, um Fehler zu korrigieren oder die Sammlung zu erweitern.

Hier beschreibe ich wie es geht.

## Installation

- [Installiere Anki auf deinem Computer](https://apps.ankiweb.net/#download)

## Vokabeln herunterladen

- [Lade die Vokabeln herunter](https://ankiweb.net/shared/info/712914300?cb=1708975654260)
- Nach dem Download, öffne die Datei per Doppelklick in Anki

## Sammlung mit AnkiWeb und dem Smartphone synchronisieren

Am einfachsten ist die Synchronisation mit einem Konto auf AnkiWeb. (Falls du
Bedenken hast, deine Sammlung auf AnkiWeb hochzuladen, gibt es auch die
Möglichkeit die Sammlung auf dem Computer zu exportieren und im Smartphone zu
importieren.)

Hier der einfach Weg über AnkiWeb:

- [Erstelle ein AnkiWeb Konto](https://ankiweb.net/account/signup)
- Synchronisiere die Sammlung auf deinem Smartphone mit AnkiWeb:

![Synchronisiere die Sammlung auf deinem Smartphone mit AnkiWeb](doc/images/AnkiDroid_sync.png){width=40%}

1. Tippe auf das Synchronisieren Symbol oben rechts
2. Einloggen und den Anweisungen folgen

- Synchronisiere deine Sammlung auf dem Computer mit AnkiWeb:

![Synchronisiere deine Sammlung auf dem Computer mit AnkiWeb](doc/images/anki_sync.png)

# Lizenz

[CC BY-SA 4.0 Deed - Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/)
